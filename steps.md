mkdir django-todo
cd django-todo

pipenv shell
pipenv install django

django-admin startproject backend

cd backend
python manage.py startapp todo
python manage.py migrate
python manage.py runserver

Odemo u otvorimo backend/settings.py i dodamo u INSTALLED_APPS 'todo',

Zatim odemo u todo/models.py i definiramp model u bazi

class Todo(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    completed = models.BooleanField(default=False)

    def _str_(self):
        return self.title

Onda pokrenemo migracije 
python manage.py makemigrations todo

Onda te migracije apliciramo na bazu 
python manage.py migrate todo

Zatim u todo/admin.py dodamo 

from django.contrib import admin
from .models import Todo

class TodoAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', 'completed')

# Register your models here.

admin.site.register(Todo, TodoAdmin)

Onda kreiramo superusera da mozemo koristit admin interface

python manage.py createsuperuser

pokrenemo server opet 
python manage.py runserver

// frontend
pip install djangorestframework django-cors-headers

u backend/settings.py

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'rest_framework',
    'todo',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]

CORS_ORIGIN_WHITELIST = [
     'http://localhost:3000'
]

backend/serializer.py
from rest_framework import serializers
from .models import Todo

class TodoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Todo
        fields = ('id', 'title', 'description', 'completed')

