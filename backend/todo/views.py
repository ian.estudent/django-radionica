from ast import Str
from multiprocessing import context
from urllib import response
from django.shortcuts import render
from rest_framework import viewsets
from .serializers import TodoSerializer
from .models import Todo
from django.http import HttpResponse

# Create your views here.

class TodoView(viewsets.ModelViewSet):
    serializer_class = TodoSerializer
    queryset = Todo.objects.all()

def index(request):
    todo_list = Todo.objects.order_by('end_date')[:5]
    output = '<br><p style="margin-left: 20px">'.join([t.title for t in todo_list]) + '<p>'
    return HttpResponse(output)