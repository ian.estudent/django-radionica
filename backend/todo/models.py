from django.db import models
import django.utils.timezone


# Create your models here.
class Todo(models.Model):
    title = models.CharField(max_length=120)
    description = models.TextField()
    completed = models.BooleanField(default=False)
    start_date = models.DateTimeField(default=django.utils.timezone.now)
    end_date= models.DateTimeField(default=django.utils.timezone.now)

    def _str_(self):
        return self.title