### Zadatak:
#### 1. Dodajte mogucnost dodavanja vremena zavrsetka i pocetka todo taska.
Format koji treba bit spremljen u bazu treba bit 'godina-mjesec-dan sat-minuta'
Tu vam dajem malo prostora za razmisljanje kako i sta napravit. Ideja je da se sami potrudite napravit to kako god znate i umijete, a kasnije vam ja komentiram zasto je to dobro ili lose i kako bi moglo bolje.
Cilj je da nesto naucite radeci i razmisljajuci kako napraviti nesto. Takav slicni zadatak ce te dobiti na poslu tako da vam je ovo dobra prilika da vidite kako to ide.
Tips: Googlajte django dokumentaciju

#### 2. Dodajte Authentication u app. 
(Zadatak 2. je za backend ekipu, FE mogu preskocit slobodno, ali mogu i rijesit ako ih zanima)
Poticem vas da bude full jednostavno u smislu samo username i password. Ne morate se bavit s mailovima. 
Dosta toga imate u django dokumentaciji pa bacite oko na to, ustedit cete puno vremena ako proucite i shvatite dokumentaciju (Vjeruj te mi znam iz iskustva). 
Cilj je da user moze pristupiti samo todo taskovima koje je on napravio, a da ostale ne vidi.

Zadatak radite tako da kreirate svoj repozitorij na svom privatnom gitlabu. Kao npr. https://gitlab.com/petar.klenovic da vam stoji ovako na profilu kao meni js-hackaton.
Moze te klonat ovaj repo i prebacit si ga na svoj profil ili iz onog sto smo radili na radionici napravite git repositorij.
> ##### Rok je do 7.2.2022. 23:59
Ako negdje stvarno zapnete javite pa cemo proc kroz kod malo da vidimo di je problem.

Pokretanje projekta:
`git clone https://gitlab.com/eSTUDENT/todo-app-django`
`cd todo-app-django`

nemojte zaboravit uc u virtualni env
`pipenv shell`
instalirajte dependencies
`pipenv install`

pokrenite server
`python manage.py runserver  `